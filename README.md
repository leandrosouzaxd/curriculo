# Leandro de Souza Silva

*27 anos*

*União Estável*


Fortaleza/CE

---

## Contatos


**Telefone:** (85) 98541-3220

**E-mail:** leandroxd27@gmail.com

**Linkedin:** [https://www.linkedin.com/in/leandro-souza-b70854138/](https://www.linkedin.com/in/leandro-souza-b70854138/)

**Github:** leandrosouzaxd

**Skype:** leandrosouzamg

**Inglês:** Intermediário


---

## Competências

**BackEnd:**
* Python
* Django/Flask/
* Delphi
* PHP
* Programação Orientada à Objeto


**FrontEnd:**
* HTML
* CSS
* Bootstrap
* Javascript


**Banco de Dados:**
* MySQL
* SQLite
* Oracle


**Outros:**
* Padrões de Projeto *(Modelo MVC)*
* Sistema de Controle de versão *(git)*
* Conhecimento em Design e Arquitetura de Software
* Scrum
* Kanban



---

## Informações adicionais

* Experiência em trabalhar com Produto.
* Prezo muito pela qualidade do código.
* Facilidade e interesse em estar sempre aprendendo e evoluíndo.
* Prazer em passar conhecimento adiante.
* Criatividade.
* Determinação e responsabilidade.
* Facilidade para trabalhar em grupo

---

## Experência Profissional

* **Projetos Pessoais**
> Trabalho em alguns projetos de portal criando alguns serviços de crawler para raspagem de dados. 
> Pequeno projeto opensource ainda em andamento inicial para catalogo eletronico ,pra qualquer um que tenha o ERP winthor poder colocar no seu site.
> Desenvolvimento de scripts de automação de tarefas com Python.


* 2018 / Dias Atuais - **CSL Distribuidora**
> Gestão de Projetos.
Atuo nas implantaçoes de novas ferramentas e Novos processos.

* 2018 - **Freelancer Alcode**
> Desenvolvimento de Rotina importação de pedido em Excel(Delphi)

* 2017 [Contrato] - **VB Distribuidora e Mega Comercial**
> Consultor autonomo, Desenvolvimento de scripts de automação de tarefas com Python, buscava soluções na area de TI em geral , desde Firewall,CFTvs ..ETc
(ERP-Winthor)

* 2016/2017 [Contrato]	- **MS PRO Consultoria**
> Atuei como consultor,Implantação de modulos customizados, treinamentos (ERP - Winthor).

* 2016[CLT] - **Ramacon**
> Tirava duvidas dos usuarios referente ao ERP(winthor),atualização das rotinas e servidor NF-e,estudava novas rotinas pra 
agilidade nos processos, relatorios customizados,Firewall ,redes e manutenção de computadores.DNS.

* 2012/2016 [CLT] – **MG Distribuidora**
> Tirava duvidas dos usuarios referente ao ERP(winthor),atualização das rotinas e servidor NF-e,estudava novas rotinas pra 
agilidade nos processos, relatorios customizados 


---

## Formação

* **Alura Cursos** - Desenvolvimento em Python Web
* **Alura Cursos** - HTML5 e CSS
* **Alura Cursos** - Javascript
* **Devmedia** - Github
* **Devmedia** - Exportando dados para o Excel com Delphi ComObj
* **Devmedia** - Delphi: Cadastro Orientado a Objetos com FireDAC
* **Devmedia** – Começando no Delphi: Controles visuais .
* **Devmedia** – Django REST: Criando uma API web.

